package com.meisen.springboot.repo;

import com.meisen.springboot.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * 文章仓储
 * @author meisen
 * 2018-04-18
 */
public interface ArticleRepository extends JpaRepository<Article, String> {

    List<Article> findAllByIdNotNullOrderByVisitDesc(Pageable pageable);

    List<Article> findAllByIdNotNullOrderByDateDesc(Pageable pageable);

    List<Article> findArticlesByTagsLikeOrderByDateDesc(String tags, Pageable pageable);

    @Modifying
    @Query("update Article a set a.visit=?1 where a.id=?2")
    void updateArticleVisitById(int visit, String id);

    @Modifying
    @Query("update Article a set a.compliment=?1 where a.id=?2")
    void updateArticleComplimentById(int visit, String id);
}
