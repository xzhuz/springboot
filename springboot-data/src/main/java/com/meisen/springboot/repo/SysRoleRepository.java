package com.meisen.springboot.repo;

import com.meisen.springboot.entity.SysRole;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * 系统角色仓储
 * @author meisen
 * 2018-08-03
 */
public interface SysRoleRepository extends CrudRepository<SysRole, Integer> {

}
