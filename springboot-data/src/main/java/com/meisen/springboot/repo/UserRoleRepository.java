package com.meisen.springboot.repo;

import com.meisen.springboot.entity.UserRole;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * 用户角色对应关系仓储
 * @author meisen
 * 2018-08-03
 */
public interface UserRoleRepository extends CrudRepository<UserRole, Integer> {

    List<UserRole> findByUid(String Uid);

    UserRole findByRoleId(Integer roleId);
}
