package com.meisen.springboot.repo;

import com.meisen.springboot.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * 用户仓储
 * @author meisen
 * 2018-05-03
 */
public interface UserRepository extends CrudRepository<User, String> {

    Optional<User> findByUsername(String username);
}
