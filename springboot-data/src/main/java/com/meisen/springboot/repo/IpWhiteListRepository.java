package com.meisen.springboot.repo;

import com.meisen.springboot.entity.IpWhiteList;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ip白名单
 * @author meisen
 * 2018-05-09
 */
public interface IpWhiteListRepository extends JpaRepository<IpWhiteList, String> {

    void deleteIpWhiteListByIp(String Ip);
}
