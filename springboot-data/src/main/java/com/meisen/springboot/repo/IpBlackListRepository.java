package com.meisen.springboot.repo;

import com.meisen.springboot.entity.IpBlackList;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Ip黑名单
 * @author meisen
 * 2018-05-08
 */
public interface IpBlackListRepository extends JpaRepository<IpBlackList, String> {

    IpBlackList findByIp(String ip);

    void deleteIpBlackListByIp(String ip);
}
