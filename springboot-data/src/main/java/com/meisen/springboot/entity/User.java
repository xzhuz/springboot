package com.meisen.springboot.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * 用户实体类
 * @author meisen
 * 2018-04-18
 */
@Entity
public class User implements Serializable {

    public static final String NO_SUCH_USER = "no-such-user";

    @Id
    private String uid = UUID.randomUUID().toString().replaceAll("-", "");

    @Column(nullable = false, length = 200, unique = true)
    private String username;

    @Column(nullable = false, length = 500)
    private String password;

    @Column(length = 100)
    private String date = LocalDateTime.now().toString();

    // 盐
    private String salt;


    private byte state; //用户状态,0: 正常用户 , 1:账户冻结

    @ManyToMany(fetch= FetchType.EAGER)//立即从数据库中进行加载数据;
    @JoinTable(name = "UserRole", joinColumns = { @JoinColumn(name = "uid") }, inverseJoinColumns ={@JoinColumn(name = "roleId") })
    private List<SysRole> roleList;// 一个用户具有多个角色

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public List<SysRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SysRole> roleList) {
        this.roleList = roleList;
    }

    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }

    public String getCredentialsSalt() {
        return this.username + this.salt;
    }

    public String getAuthCacheKey() {
        return getUid();
    }
}
