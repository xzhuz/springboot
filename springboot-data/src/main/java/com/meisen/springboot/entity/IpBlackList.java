package com.meisen.springboot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * ip黑名单
 * @author meisen
 * 2018-05-08
 */
@Entity
public class IpBlackList implements Serializable {

    @Id
    private String ip;

    @Column(length = 50, nullable = false)
    private String date = LocalDateTime.now().toString();


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
