package com.meisen.springboot.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * 文章归档数据
 * @author meisen
 * 2018-07-10
 */
public class AchieveData  implements Serializable {

    private String id;
    private String title;
    private Date date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AchieveData article = (AchieveData) o;
        return Objects.equals(id, article.id) &&
                Objects.equals(date, article.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date);
    }
}
