package com.meisen.springboot.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * 文章实体类
 * @author meisen
 * 2018-04-18
 */
@Entity
public class Article implements Serializable {

    public static final String NO_SUCH_ARTICLE = "NO_SUCH_ARTICLE";

    /**
     * 主键 id 通过UUID 生产
     */
    @Id
    private String id = UUID.randomUUID().toString().replaceAll("-", "");

    /**
     * 文章头像
     */
    @Column(nullable = false, length = 600)
    private String thumb;

    /**
     * 文章题目
     */
    @Column(nullable = false, length = 200)
    private String title;

    /**
     * 简介总结
     */
    @Column(length = 400)
    private String summary;

    /**
     * 文章内容
     */
    @Column(columnDefinition = "TEXT")
    private String content;

    /**
     * 文章标签tags
     */
    @Column(length = 200)
    private String tags;

    /**
     * 文章访问次数
     */
    @Column(columnDefinition = "INT", updatable = false)
    private Integer visit = 0;

    /**
     * 文章点赞数
     */
    @Column(columnDefinition = "INT")
    private Integer compliment = 0;

    /**
     * 文章是否发布 true 发布  false 未发布
     */
    @Column
    private Boolean publish;

    /**
     * 文章发布时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    private Date date;

    /**
     * 更新时间
     */
    @Column(name="updated")
    @org.hibernate.annotations.UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    @Column(updatable = false)
    public Integer getVisit() {
        return visit;
    }

    public void setVisit(Integer visit) {
        this.visit = visit;
    }

    public Boolean isPublish() {
        return publish;
    }

    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

    @Column(updatable=false)
    public Integer getCompliment() {
        return compliment;
    }

    public void setCompliment(Integer compliment) {
        this.compliment = compliment;
    }

    public Boolean getPublish() {
        return publish;
    }

    @Column(updatable=false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return Objects.equals(id, article.id) &&
                Objects.equals(date, article.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date);
    }
}
