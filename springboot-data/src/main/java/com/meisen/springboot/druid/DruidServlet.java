package com.meisen.springboot.druid;

import com.alibaba.druid.support.http.StatViewServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

/**
 * 配置监控页面的账号密码，白名单黑名单之类
 * @author meisen
 * 2018-04-23
 */
@WebServlet(urlPatterns = "/druid/*", initParams = {
        @WebInitParam(name = "allow", value = ""), // ip白名单（没有配置或为空，则允许所有访问）
        @WebInitParam(name = "deny", value = ""), // IP黑名单 （deny优先于allow)
        @WebInitParam(name = "loginUsername", value = "admin"), // 登陆Druid管理页面用户名
        @WebInitParam(name = "loginPassword", value = "admin") // 登陆Druid 管理页面密码
})
public class DruidServlet extends StatViewServlet {
}
