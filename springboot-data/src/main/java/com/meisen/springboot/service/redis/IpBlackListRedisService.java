package com.meisen.springboot.service.redis;

import com.meisen.springboot.entity.IpBlackList;
import com.meisen.springboot.service.HashRedisService;
import org.springframework.stereotype.Service;

@Service
public class IpBlackListRedisService extends HashRedisService<IpBlackList> {

    private static final String IP_HASH_KEY = "IP-HASH-KEY";

    @Override
    protected String getHashKey() {
        return IP_HASH_KEY;
    }
}
