package com.meisen.springboot.service.impl;

import com.meisen.springboot.entity.UserRole;
import com.meisen.springboot.repo.UserRoleRepository;
import com.meisen.springboot.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author meisen
 * 2018-08-03
 */
@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public UserRole configUserRole(UserRole userRole) {
        return userRoleRepository.save(userRole);
    }

    @Override
    public UserRole addUserRole(UserRole userRole) {
        return userRoleRepository.save(userRole);
    }

    @Override
    public List<UserRole> findUserRoleByUid(String uid) {
        return userRoleRepository.findByUid(uid);
    }

    @Override
    public UserRole findUserRoleByRoleId(Integer roleId) {
        return userRoleRepository.findByRoleId(roleId);
    }
}
