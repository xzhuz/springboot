package com.meisen.springboot.service.redis;

import com.meisen.springboot.service.HashRedisService;
import org.springframework.stereotype.Service;

/**
 * @author meisen
 * 2018-05-06
 */
@Service
public class ArticleTagRedisService extends HashRedisService<String> {

    private static final String TAG_HASH_KEY = "TAG-HASH-KEY";

    @Override
    public String getHashKey() {
        return TAG_HASH_KEY;
    }
}
