package com.meisen.springboot.service;

import com.meisen.springboot.entity.SysRole;

import java.util.List;

/**
 * @author meisen
 * 2018-08-03
 */
public interface SysRoleService {

    /**
     * 添加新角色
     * @param role 角色
     * @return 新角色
     */
    SysRole addRole(SysRole role);

    /**
     * 按照role查询
     * @param ids roleId 集合
     * @return List<SysRole>
     */
    List<SysRole> findAllByIds(List<Integer> ids);
}
