package com.meisen.springboot.service.redis;

import com.meisen.springboot.service.SetRedisService;
import org.springframework.stereotype.Service;

/**
 * 发布文章索引
 *
 * @author meisen
 * 2018-05-06
 */
@Service
public class ArticlePublishRedisService extends SetRedisService<String> {


    private static final String PUBLISH_ARTICLE_KEY = "PUBLISH_ARTICLE_KEY";

    @Override
    public String getSetKey() {
        return PUBLISH_ARTICLE_KEY;
    }
}
