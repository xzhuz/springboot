package com.meisen.springboot.service;

import com.meisen.springboot.entity.UserRole;

import java.util.List;

/**
 * @author meisen
 * 2018-08-03
 */
public interface UserRoleService {

    /**
     * 配置用户角色
     * @param userRole 角色配置
     * @return 新用户角色
     */
    UserRole configUserRole(UserRole userRole);

    /**
     * 添加新用户角色
     * @param userRole 用户角色
     * @return 新用户角色
     */
    UserRole addUserRole(UserRole userRole);

    /**
     * 根据用户id查询
     * @param uid 用户id
     * @return List<UserRole>
     */
    List<UserRole> findUserRoleByUid(String uid);

    /**
     * 根据角色id查询
     * @param roleId 角色id
     * @return UserRole
     */
    UserRole findUserRoleByRoleId(Integer roleId);
}
