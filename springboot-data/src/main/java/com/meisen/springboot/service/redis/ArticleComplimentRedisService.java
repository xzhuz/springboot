package com.meisen.springboot.service.redis;

import com.meisen.springboot.service.ZSetRedisService;
import org.springframework.stereotype.Service;

/**
 * 点赞数
 * @author meisen
 * 2018-05-19
 */
@Service
public class ArticleComplimentRedisService extends ZSetRedisService<String> {

    private static final String ZSET_KEY = "COMPLIMENT-ZSET-KEY";

    @Override
    protected String getZSetKey() {
        return ZSET_KEY;
    }
}
