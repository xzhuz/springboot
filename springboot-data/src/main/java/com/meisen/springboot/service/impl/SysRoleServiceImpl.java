package com.meisen.springboot.service.impl;

import com.meisen.springboot.entity.SysRole;
import com.meisen.springboot.repo.SysRoleRepository;
import com.meisen.springboot.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SysRoleServiceImpl implements SysRoleService {

    private final SysRoleRepository sysRoleRepository;

    @Autowired
    public SysRoleServiceImpl(SysRoleRepository sysRoleRepository) {
        this.sysRoleRepository = sysRoleRepository;
    }

    @Override
    public SysRole addRole(SysRole role) {
        return sysRoleRepository.save(role);
    }

    @Override
    public List<SysRole> findAllByIds(List<Integer> ids) {
        return (List<SysRole>) sysRoleRepository.findAllById(ids);
    }
}
