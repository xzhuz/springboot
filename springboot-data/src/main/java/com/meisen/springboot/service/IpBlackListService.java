package com.meisen.springboot.service;

import com.meisen.springboot.entity.IpBlackList;

import java.util.List;

/**
 * ip访问控制
 * @author meisen
 * 2018-05-08
 */
public interface IpBlackListService {

    /**
     * 查询所有黑名单
     * @return 所有黑名单
     */
    List<IpBlackList> findAll();

    /**
     * 根据Ip查询
     * @param  ip 根据Ip查询
     * @return 黑名单
     */
    IpBlackList findByIp(String ip);

    /**
     * 添加黑名单记录
     * @param ipBlackList 黑名单
     */
    void addIpBlackList(IpBlackList ipBlackList);

    /**
     * 删除黑名单
     * @param ip ip
     */
    void deleteByIp(String ip);

    /**
     * 清空缓存
     */
    void clearCache();
}
