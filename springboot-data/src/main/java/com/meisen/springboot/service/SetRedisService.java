package com.meisen.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;

import javax.annotation.Resource;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 缓存Redis接口类
 * 类型 Set
 * @author meisen
 * 2018-05-05
 */
public abstract class SetRedisService<T> {
    /**
     * 实例化 RedisTemplate对象
     */
    @Autowired
    protected RedisTemplate<String, Object> redisTemplate;

    /**
     * SetOperations
     */
    @Resource
    private SetOperations<String, T> setOperations;

    /**
     * 定义Set集合的Set key名称
     *
     * @return key名称
     */
    protected abstract String getSetKey();

    /**
     * 默认添加值
     * @param values 值
     */
    public void sAdd(T[] values, long expire) {
        setOperations.add(getSetKey(), values);
        if (expire != -1) {
            redisTemplate.expire(getSetKey(), expire, TimeUnit.SECONDS);
        }
    }

    /**
     * 添加指定key
     * @param key 指定key
     * @param values 值
     */
    public void addSet(String key, T[] values, long expire) {
        setOperations.add(key, values);
        if (expire != 0 && expire != -1) {
            redisTemplate.expire(key, expire, TimeUnit.SECONDS);
        }
    }

    /**
     * 获取默认所有的元素
     * @return 所有元素
     */
    public Set<T> allSet() {
        return allSet(getSetKey());
    }

    /**
     * 获取指定key所有元素
     * @param key 指定key
     * @return 所有元素
     */
    public Set<T> allSet(String key) {
        return setOperations.members(key);
    }

    /**
     * 获取默认所有
     * @return 默认所有
     */
    public Set<T> getAllSet() {
        return getAllSetByKey(getSetKey());
    }

    /**
     * 根据key获取所有
     * @param key key
     * @return 所有元素
     */
    public Set<T> getAllSetByKey(String key) {
        return setOperations.members(key);
    }

    /**
     * 清空默认set缓存
     */
    public void emptySet() {
       emptySet(getSetKey());
    }

    /**
     * 判断key是否存在
     * @param key key
     * @return true: 不存在 false存在
     */
    public boolean isSetKeyExists(String key) {
        Set<T> set = setOperations.members(key);
        return set.isEmpty();
    }


    /**
     * 清空指定key缓存
     * @param key 指定key
     */
    public void emptySet(String key) {
        Set<T> defaultSet = setOperations.members(key);
        defaultSet.forEach(set -> setOperations.remove(key, set));
    }

}
