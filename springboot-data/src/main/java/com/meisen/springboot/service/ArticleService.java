package com.meisen.springboot.service;

import com.meisen.springboot.entity.AchieveData;
import com.meisen.springboot.entity.Article;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 文章Service类
 * @author meisen
 * 2018-04-18
 */
@Service
public interface ArticleService {

    /**
     * 获取所有文章
     * @return 文章列表
     */
    List<Article> findAll();

    /**
     * 归档
     * @return 所有文件
     */
    Map<String, List<AchieveData>> achieveArticle();

    /**
     * 发布文章
     * @param article 需要发布的文章
     * @return Article
     */
    Article publishArticle(Article article);

    /**
     * 根据id删除文章
     * @param id id
     */
    void deleteArticle(String id);

    /**
     * 更新文章
     * @param article 新文章
     * @return 更新后的文章
     */
    Article updateArticle(Article article);

    /**
     * 增加访问量
     * @param id 文章id
     * @param visit 文章访问量
     */
    void increaseVisit(String id, Integer visit);

    /**
     * 点赞
     * @param id 需要点赞的id
     * @param compliment 点赞数
     */
    void compliment(String id, Integer compliment);

    /**
     * 取消点赞
     * @param id id
     * @param compliment 点赞数
     */
    void cancelCompliment(String id, Integer compliment);

    /**
     * 获取文章访问量
     * @param id 文章id
     * @return 文章访问量
     */
    Integer getArticleVisit(String id);

    /**
     * 获取文章点赞数
     * @param id 文章id
     * @return 点赞数
     */
    Integer getArticleCompliment(String id);

    /**
     * 根据id获取文章
     * @param id id
     * @return 文章内容
     */
    Article getArticleById(String id);

    /**
     * 统计已发布文章数
     * @param publish 统计已发布文章
     * @return 文章数量
     */
    Long countArticle(boolean publish);

    /**
     * 统计所有文章
     * @return 所有文章数量
     */
    Integer countArticle();

    /**
     * 热门文章
     * @param page 当前页数
     * @param size 每页数量
     * @return 热门文章
     */
    List<Article> findPopularArticles(Integer page, Integer size);

    /**
     * 找到部分文章
     * @param page 查询页数
     * @param size 每页展示条数
     * @return 文章
     */
    List<Article> findPartArticles(int page, int size);

    /**
     * 相关文章
     * @param tags 标签
     * @param page 查询页数
     * @param size 每页展示条数
     * @return 相关文章
     */
    List<Article> findRelativeArticles(String tags, int page, int size);

    /**
     * 获取所有标签
     * @return 所有标签 List
     */
    List<String> getAllArticleTags();

}
