package com.meisen.springboot.service.redis;

import com.meisen.springboot.entity.IpWhiteList;
import com.meisen.springboot.service.HashRedisService;
import org.springframework.stereotype.Service;

@Service
public class IpWhiteListRedisService extends HashRedisService<IpWhiteList> {

    private static final String IP_WHITE_KEY = "IP-WHITE-KEY";

    @Override
    protected String getHashKey() {
        return IP_WHITE_KEY;
    }
}
