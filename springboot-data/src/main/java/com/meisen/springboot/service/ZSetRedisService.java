package com.meisen.springboot.service;

import org.springframework.data.redis.core.ZSetOperations;

import javax.annotation.Resource;
import java.util.Set;

/**
 * 缓存Redis抽象类
 * 类型 ZSet
 * 接口定义了获取 ZSetOperations 的抽象方法zSetOperations(), 该方法必须实现, 并返回实例
 * ZSet的key通过抽象方法getZSetKey(), 该抽象方法必须实现, 返回key
 * @author meisen
 * 2018-05-04
 */
public abstract class ZSetRedisService<T> {

    /**
     * 获取ZSetOperations操作
     */
    @Resource
    private ZSetOperations<String, T> zSetOperations;

    /**
     * 定义ZSet表的ZSet key名称
     *
     * @return key名称
     */
     protected  abstract String getZSetKey();

    /**
     * 添加缓存
     * @param t 对象
     */
    public void add(T t, double source) {
        zSetOperations.add(getZSetKey(), t, source);
    }

    /**
     * 统计数量
     * @return 数量
     */
    public Long size() {
        return zSetOperations.size(getZSetKey());
    }

    /**
     * 排序，从大到小
     * @param start 开始
     * @param end 结束
     * @return 给定范围排序的集合
     */
    public Set<T> sort(long start, long end) {
        return zSetOperations.reverseRange(getZSetKey(), start, end);
    }

    /**
     * 获取所有集合
     * @return 所有集合
     */
    public Set<T> all() {
        return zSetOperations.range(getZSetKey(), 0, -1);
    }

    /**
     * 删除集合中指定元素
     * @param id 需要删除的id
     */
    public void removeZSet(T id) {
        zSetOperations.remove(getZSetKey(), id);
    }

    /**
     * 清空ZSet缓存集合
     */
    public void emptyZSet() {
        Set<T> set = zSetOperations.range(getZSetKey(), 0, -1);
        set.forEach(id -> zSetOperations.remove(getZSetKey(), id));
    }

    /**
     * 返回有序集合中的source
     * @param t 值
     * @return score
     */
    public double score(T t) {
        return zSetOperations.score(getZSetKey(), t);
    }

    /**
     * 获取排名
     * @param t 值
     * @return 排名
     */
    public double rank(T t) {
        return zSetOperations.rank(getZSetKey(), t);
    }

}
