package com.meisen.springboot.service.redis;

import com.meisen.springboot.entity.User;
import com.meisen.springboot.service.HashRedisService;
import org.springframework.stereotype.Service;

/**
 * 用户redis缓存
 * @author meisen
 * 2018-05-08
 */
@Service
public class UserRedisService extends HashRedisService<User> {

    private static final String USER_HASH_KEY = "USER-HASH-KEY";

    @Override
    protected String getHashKey() {
        return USER_HASH_KEY;
    }
}
