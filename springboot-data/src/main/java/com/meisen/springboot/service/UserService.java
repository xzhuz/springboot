package com.meisen.springboot.service;

import com.meisen.springboot.entity.User;

import java.util.List;

/**
 * 用户服务接口，定义用户操作
 * @author meisen
 * 2018-04-18
 */
public interface UserService {
    /**
     * 注册用户
     * @param user 新用户
     * @return 注册后的用户
     */
    User registerUser(User user);

    /**
     * 更新用户
     * @param user 需要更新的用户
     * @return 更新后的用户
     */
    User saveOrUpdateUser(User user);

    /**
     * 根据id获取用户
     *
     * @param id user id
     * @return User
     */
    User findUserById(String id);

    /**
     * 根据用户名获取用户
     * @param username 用户名
     * @return 用户
     */
    User findUserByUsername(String username);


    /**
     * 根据id删除用户
     * @param id 用户id
     */
    void removeUser(String id);

    /**
     * 获取所有用户
     * @return 所有用户
     */
    List<User> findAll();
}
