package com.meisen.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redis hash类型操作
 * @author meisen
 * 2018-05-01
 */
public abstract class HashRedisService<T> {
    /**
     * 实例化 RedisTemplate对象
     */
    @Autowired
    protected RedisTemplate<String, Object> redisTemplate;
    /**
     * 定义Hash结构 操作存储实体对象
     */
    @Resource
    private HashOperations<String, String, T> hashOperations;


    /**
     * 定义Hash表的hash key名称
     *
     * @return key名称
     */
     protected abstract String getHashKey();

    /**
     * 在相应Hash表中添加键值对 filed:Object(domain)
     *
     * @param filed    key
     * @param t 对象
     * @param expire 过期时间(单位:秒),传入 -1 时表示不设置过期时间
     */
    public void put(String filed, T t, long expire) {
        hashOperations.put(getHashKey(), filed, t);
        if (expire != -1) {
            redisTemplate.expire(getHashKey(), expire, TimeUnit.SECONDS);
        }
    }

    /**
     * 在相应Hash表中删除filed名称的元素
     * @param filed 传入filed的名称
     */
    public void remove(String filed) {
        hashOperations.delete(getHashKey(), filed);
    }

    /**
     * 在相应Hash表中查询key名称的元素
     *
     * @param filed 查询的filed
     * @return 元素
     */
    public T get(String filed) {
        return hashOperations.get(getHashKey(), filed);
    }

    /**
     * 获取在相应Hash表下的所有实体对象
     *
     * @return List
     */
    public List<T> getAll() {
        return hashOperations.values(getHashKey());
    }

    /**
     * 查询在相应Hash表下的所有key名称
     *
     * @return Set
     */
    public Set<String> getKeys() {
        return hashOperations.keys(getHashKey());
    }

    /**
     * 判断在相应Hash表下key是否存在
     *
     * @param filed 传入key的名称
     * @return true |  false
     */
    public boolean isKeyExists(String filed) {
        return hashOperations.hasKey(getHashKey(), filed);
    }

    /**
     * 查询相应Hash表的缓存数量
     *
     * @return long
     */
    public long count() {
        return hashOperations.size(getHashKey());
    }

    /**
     * 清空相应Hash表的所有缓存
     */
    public void emptyHash() {
        Set<String> set = hashOperations.keys(getHashKey());
        set.forEach(filed -> hashOperations.delete(getHashKey(), filed));
    }

}
