package com.meisen.springboot.service;

import com.meisen.springboot.entity.IpWhiteList;

import java.util.List;

/**
 * ip白名单
 * @author meisen
 * 2018-05-09
 */
public interface IpWhiteListService {

    /**
     * 所有IpWhiteList
     * @return IpWhiteLists
     */
    List<IpWhiteList> findAll();

    /**
     * 所有ip
     * @return 白名单
     */
    List<String> findAllIpWhite();

    /**
     * 添加白名单记录
     * @param ipWhiteList 黑名单
     */
    void addIpBlackList(IpWhiteList ipWhiteList);

    /**
     * 删除白名单
     * @param ip ip
     */
    void deleteIpWhiteList(String ip);

    /**
     * 清空缓存
     */
    void clearCache();
}
