package com.meisen.springboot.service.redis;

import com.meisen.springboot.entity.Article;
import com.meisen.springboot.service.HashRedisService;
import org.springframework.stereotype.Service;

/**
 * 文章缓存实现
 * 通过ZSet来管理
 *
 * @author meisen
 * 2018-05-04
 */
@Service
public class ArticleRedisService extends HashRedisService<Article> {

    private static final String HASH_KEY = "ARTICLE-HASH-KEY";


    public String getHashKey() {
        return HASH_KEY;
    }
}
