package com.meisen.springboot.service.redis;

import com.meisen.springboot.service.ZSetRedisService;
import org.springframework.stereotype.Service;

/**
 * 文章访问量id集合
 *
 * @author meisen
 * 2018-05-06
 */
@Service
public class ArticleVisitRedisService extends ZSetRedisService<String> {


    private static final String ARTICLE_VISIT_KEY = "ARTICLE-VISIT-KEY";

    @Override
    public String getZSetKey() {
        return ARTICLE_VISIT_KEY;
    }
}
