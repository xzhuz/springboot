package com.meisen.springboot.service.redis;

import com.meisen.springboot.entity.AchieveData;
import com.meisen.springboot.service.HashRedisService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author meisen
 * 2018-05-06
 */
@Service
public class ArticleAchieveRedisService extends HashRedisService<List<AchieveData>> {

    private static final String ACHIEVE_HASH_KEY = "ACHIEVE_HASH_KEY";

    @Override
    public String getHashKey() {
        return ACHIEVE_HASH_KEY;
    }
}
