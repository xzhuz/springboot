package com.meisen.springboot.service.redis;

import com.meisen.springboot.service.HashRedisService;
import org.springframework.stereotype.Service;


@Service
public class ConfigRedisService extends HashRedisService<Config> {

    private static final String CONFIG_HASH_KEY = "CONFIG-HASH-KEY";

    @Override
    public String getHashKey() {
        return CONFIG_HASH_KEY;
    }
}
