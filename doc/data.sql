
create table config (
  id varchar(50) NOT NULL primary key ,
  `key` varchar(20) not null unique,
  value varchar(30) not null
) DEFAULT CHARSET=utf8;

create table user (
  id varchar(50) NOT NULL primary key ,
  username varchar(50) not null unique,
  password varchar(100) not null,
  salt varchar(100) not null,
  state int not null,
  date varchar(100)
) DEFAULT CHARSET=utf8;


-- auto-generated definition
create table sys_role
(
  id int not null primary key,
  available   bit          null,
  description varchar(500) null,
  role        varchar(500) not null
)DEFAULT CHARSET=utf8;


create table user_role (
  id int not null,
  uid varchar(50) not null,
  roleId int not null,
  FOREIGN KEY (uid) REFERENCES user(id),
  FOREIGN KEY (roleId) REFERENCES sys_role(id)
) DEFAULT CHARSET=utf8;

create table ip_white_list(
  id varchar(100) not null primary key ,
  date varchar(100)
) DEFAULT CHARSET=utf8;


create table ip_black_list(
  id varchar(100) not null primary key ,
  date varchar(100)
) DEFAULT CHARSET=utf8;


create table article (
  id varchar(50) NOT NULL primary key comment '文章唯一标识id',
  thumb varchar(600) comment '文章页面头像链接',
  title varchar(200) not null comment '文章题目',
  summary varchar(400) comment '文章总结',
  tags varchar(200) comment '文章标签',
  content text comment '文章内容',
  visit int default 0 comment '文章访问量',
  compliment int DEFAULT 0 comment '文章喜欢数量',
  publish boolean default false comment '文章是否发布 true： 已发布 false: 文章未发布',
  date timestamp DEFAULT CURRENT_TIMESTAMP comment '文章发布时间',
  updated timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '文章更新时间'
) DEFAULT CHARSET=utf8;