create table article (
  id varchar(50) NOT NULL primary key comment '文章唯一标识id',
  thumb varchar(600) comment '文章页面头像链接',
  title varchar(200) not null comment '文章题目',
  summary varchar(400) comment '文章总结',
  tags varchar(200) comment '文章标签',
  content text comment '文章内容',
  visit int default 0 comment '文章访问量',
  compliment int DEFAULT 0 comment '文章喜欢数量',
  publish boolean default false comment '文章是否发布 true： 已发布 false: 文章未发布',
  date timestamp DEFAULT CURRENT_TIMESTAMP comment '文章发布时间',
  updated timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '文章更新时间'
) DEFAULT CHARSET=utf8;