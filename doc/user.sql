create table user (
  id varchar(50) NOT NULL primary key ,
  username varchar(50) not null unique,
  password varchar(100) not null,
  salt varchar(100) not null,
  state int not null,
  date varchar(100)
) DEFAULT CHARSET=utf8;


INSERT INTO `sys_role` (`id`,`available`,`description`,`role`)
VALUES (1,'0','管理员','admin');

INSERT INTO `user` (`id`,`username`, `password`,`salt`,`state`)
VALUES ('1', 'admin', 'd3c59d25033dbf980d29554025c23a75', '8d78869f470951332959580424d4bf4f', 0);

INSERT INTO `user_role` (`role_id`,`uid`)
VALUES (1,1);
