package com.meisen.springboot.util;

import com.meisen.springboot.response.ResultEntity;

/**
 * 统一返回工具类
 * @author meisen
 * 2018-04-18
 */
public class Result {

    /**
     * 成功处理逻辑
     * @param data 返回数据
     * @param msg 返回信息
     * @return 统一返回result
     */
    public static ResultEntity<Object> success(Object data, String msg) {
        ResultEntity<Object> resultEntity = new ResultEntity<>();
        resultEntity.setCode(0);
        resultEntity.setMsg(msg);
        resultEntity.setData(data);
        return resultEntity;
    }

    /**
     * 错误处理逻辑
     * @param errCode 错误码
     * @param errorMsg 错误信息
     * @return 统一返回result
     */
    public static ResultEntity<Object> error(Integer errCode, String errorMsg) {
        ResultEntity<Object> resultEntity = new ResultEntity<>();
        resultEntity.setCode(errCode);
        resultEntity.setMsg(errorMsg);
        resultEntity.setData(null);
        return resultEntity;
    }
}
