package com.meisen.springboot.interceptor;

import com.springboot.meisen.core.Constants;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 请求拦截器
 * 拦截器需要注册
 * @author meisen
 * 2018-07-31
 */
public class RequestInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        // 添加跨域header
        response.setHeader("Access-Control-Allow-Origin", Constants.ORIGIN);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type,Access-Token,X-Requested-With,Authorization");
        response.setHeader("Access-Control-Expose-Headers", "*");
        if (RequestMethod.OPTIONS.name().equals(request.getMethod())) {
            response.setStatus(200);
            return true;
        }
        return true;
    }
}
