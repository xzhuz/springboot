package com.meisen.springboot.controller;

import com.meisen.springboot.response.ResultEntity;
import com.meisen.springboot.service.IpBlackListService;
import com.meisen.springboot.service.IpWhiteListService;
import com.meisen.springboot.util.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 缓存controller
 * @author meisen
 * 2018-05-07
 */
@RestController
@RequestMapping("/api/cache")
public class CacheController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheController.class);
    private final ConfigService configService;
    private final IpBlackListService ipBlackListService;
    private final IpWhiteListService ipWhiteListService;

    @Autowired
    public CacheController(ConfigService configService, IpBlackListService ipBlackListService, IpWhiteListService ipWhiteListService) {
        this.configService = configService;
        this.ipBlackListService = ipBlackListService;
        this.ipWhiteListService = ipWhiteListService;
    }

    @GetMapping("/config")
    public ResultEntity config() {
        configService.clearConfigCache();
        LOGGER.info("刷新Config缓存成功");
        return Result.success("刷新Config缓存成功", null);
    }

    @GetMapping("/blackList")
    public ResultEntity ipBlackList(){
        ipBlackListService.clearCache();
        LOGGER.info("刷新黑名单缓存成功");
        return Result.success("刷新黑名单缓存成功", null);
    }

    @GetMapping("/whiteList")
    public ResultEntity ipWhiteList(){
        ipWhiteListService.clearCache();
        LOGGER.info("刷新白名单缓存成功");
        return Result.success("刷新白名单缓存成功", null);
    }
}
