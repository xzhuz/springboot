package com.meisen.springboot.exception;

import com.meisen.springboot.response.ResultEntity;
import com.meisen.springboot.util.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandle {
        private final static Logger LOGGER= LoggerFactory.getLogger(ExceptionHandle.class);
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResultEntity handle(Exception e) {
        if (e instanceof ServerException) {
            ServerException serverException = (ServerException) e;
            return Result.error(serverException.getCode(), serverException.getErrorMsg());
        } else if (e instanceof RequestLimitException) {
            RequestLimitException requestLimitException = (RequestLimitException) e;
            return Result.error(3, requestLimitException.getMessage());
        }
        LOGGER.error("[系统异常]未知错误!, {}" , e.getMessage());
        return Result.error(2, "[系统异常]未知错误!");
    }

}
