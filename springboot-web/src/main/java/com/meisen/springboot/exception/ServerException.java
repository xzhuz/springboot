package com.meisen.springboot.exception;

/**
 * 服务错误组件
 * @author meisen
 * 2018-04-17
 */
public class ServerException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    private Integer code;

    private String errorMsg;

    public ServerException(Integer code) {
        this.code = code;
    }

    public ServerException(Integer code, String errorMsg) {
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public ServerException(Integer code, String errorMsg, Throwable cause ) {
        super(cause);
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
