package com.meisen.springboot.exception;

/**
 * 访问ip限制异常类
 * @author meisen
 * 2018-05-09
 */
public class RequestLimitException extends Exception {
    public RequestLimitException() {
        super("HTTP请求超出设定的限制");
    }

    public RequestLimitException(String message) {
        super(message);
    }

    public RequestLimitException(String message, Throwable cause){
        super(message, cause);
    }
}